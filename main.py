import keras
from keras.models import Sequential
from keras.layers import Conv2D
from keras import layers

import numpy as np
import cv2
import sklearn
from sklearn.model_selection import train_test_split
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib import image

import os
import random
import gc

train_dir = "./cell_images"
parasitized_dir = "{}/Parasitized".format(train_dir)
uninfected_dir = "{}/Uninfected".format(train_dir)

train_parasitized = ['{}/{}'.format(parasitized_dir, img) for img in os.listdir(parasitized_dir) if 'png' in img]
train_uninfected = ['{}/{}'.format(uninfected_dir, img) for img in os.listdir(uninfected_dir) if 'png' in img]

train_imgs = train_parasitized[:2000] + train_uninfected[:2000]

random.shuffle(train_imgs)

# for img in train_imgs[0:3]:
#     imgp = image.imread(img)
#     image_plot = plt.imshow(imgp)
#     plt.show()

nrows = 100
ncols = 100
channels = 1

def process_images(img_list):
    images = []
    labels = []

    for img in img_list:
        # print(img)
        images.append(cv2.resize(cv2.imread(img, cv2.IMREAD_GRAYSCALE), (nrows, ncols)))
        # images.append(cv2.imread(img, cv2.IMREAD_GRAYSCALE))
        # print(img)
        if 'Parasitized' in img:
            labels.append(1)
        else:
            labels.append(0)
    return np.array(images), np.array(labels)

timages, tlabels = process_images(train_imgs)
# print(timages[0])
# cv2.imshow('image',timages[0])
# cv2.waitKey(0)
# print(tlabels[0])

# print(timages.shape)
# print(tlabels.shape)

# val stand for validation
img_train, img_val, label_train, label_val = train_test_split(timages, tlabels, test_size=0.20, random_state=2)

del timages
del tlabels
del ncols
del nrows
del channels
del train_imgs
del train_uninfected
del train_parasitized
gc.collect()


ntrain = len(img_train)
nvl = len(img_val)
batch_size = 32
# print(ntrain)


# model = keras.Sequential([
#     keras.layers.Flatten(input_shape=(100,100)),
#     keras.layers.Dense(32, activation=keras.activations.relu),
#     keras.layers.Dense(64, activation=keras.activations.relu),
#     keras.layers.Dense(128, activation=keras.activations.relu),
#     keras.layers.Dense(256, activation=keras.activations.relu),
#     keras.layers.Dense(512, activation=keras.activations.relu),
#     keras.layers.Dense(2, activation=keras.activations.softmax)
# ])


model = keras.Sequential([
    layers.Reshape((100,100,1)),
    layers.Conv2D(32, (3, 3), activation='relu',input_shape=(100, 100, 1)),
    layers.MaxPooling2D((2, 2)),
    layers.Conv2D(64, (3, 3), activation='relu'),
    layers.MaxPooling2D((2, 2)),
    layers.Conv2D(128, (3, 3), activation='relu'),
    layers.MaxPooling2D((2, 2)),
    layers.Conv2D(128, (3, 3), activation='relu'),
    layers.MaxPooling2D((2, 2)),
    layers.Flatten(),
    layers.Dropout(0.5),
    layers.Dense(512, activation='relu'),
    layers.Dense(1, activation='softmax')
])

model.compile(optimizer=keras.optimizers.Adam(),
              loss='binary_crossentropy',
              metrics=['acc'])

# model.compile(optimizer='adam',
#               loss='sparse_categorical_crossentropy',
#               metrics=['accuracy'])


model.fit(img_train, label_train, epochs=1000)




